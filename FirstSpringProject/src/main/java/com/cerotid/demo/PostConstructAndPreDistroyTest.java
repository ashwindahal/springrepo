package com.cerotid.demo;

import org.springframework.context.support.FileSystemXmlApplicationContext;

public class PostConstructAndPreDistroyTest {
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext("PostConstructAndPreDistroy.xml");
		PostConstructAndPreDistroyExample hello = (PostConstructAndPreDistroyExample) context.getBean("hello");
		hello.sayHello();
		
		context.registerShutdownHook();
		
		
	}

}
