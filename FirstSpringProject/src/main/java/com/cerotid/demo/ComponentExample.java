package com.cerotid.demo;

import org.springframework.stereotype.Component;
@Component // creates a bean for me automatically lowers first lette
public class ComponentExample {

	public void sayHello() {
		System.out.println("Hello, from Sping Component testing class");
	}

}
