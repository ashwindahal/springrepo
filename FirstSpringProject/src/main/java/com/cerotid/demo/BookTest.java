package com.cerotid.demo;

import org.springframework.context.support.FileSystemXmlApplicationContext;

public class BookTest {

	public static void main(String[] args) {
		FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext("beans2.xml");
	
		EBook book = (EBook) context.getBean("eBook"); 
		System.out.println(book.toString());
		
		context.close();

	}

}
