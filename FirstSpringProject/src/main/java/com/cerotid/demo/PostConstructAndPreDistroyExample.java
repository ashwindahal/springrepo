package com.cerotid.demo;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class PostConstructAndPreDistroyExample {

	@PostConstruct
	public void afterInstantiation() {
		System.out.println("I will work just after instantiation of a new bean.");
	}
	
	@PreDestroy
	public void beforeDestruction() {
		System.out.println("I will work just before a bean in distroyed");
	}
	
	public void sayHello() {
		System.out.println("Hello, From Spring Learning");
	}
}
