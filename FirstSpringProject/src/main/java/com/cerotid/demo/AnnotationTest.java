package com.cerotid.demo;

import org.springframework.context.support.FileSystemXmlApplicationContext;

public class AnnotationTest {
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext ("beansWithContextAdded.xml");
		StudentAnnotationExample ob1 =  (StudentAnnotationExample) context.getBean("student1");
		StudentAnnotationExample  ob2 = (StudentAnnotationExample) context.getBean("student2");
		
		System.out.println(ob1.getName() + "-" + ob1.getId());
		System.out.println(ob2.getName() + "-" + ob2.getId());
		

		
		RequiredAnnotation annotation = (RequiredAnnotation) context.getBean("requiredAnnotation");
		System.out.println(annotation);
		
		
		context.close();
		

	}

}
