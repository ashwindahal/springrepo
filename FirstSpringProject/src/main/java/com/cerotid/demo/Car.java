package com.cerotid.demo;

public class Car {
	private String name;
	private int perDayCost;
	
	public int getPerDayCost() {
		return perDayCost;
	}
	public void setPerDayCost(int perDayCost) {
		this.perDayCost = perDayCost;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Car [name=" + name + ", perDayCost=" + perDayCost + "]";
	}
	
	

}
