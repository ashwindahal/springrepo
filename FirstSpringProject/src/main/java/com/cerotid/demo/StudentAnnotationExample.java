package com.cerotid.demo;

import org.springframework.beans.factory.annotation.Value;

public class StudentAnnotationExample {
	@Value("john Doe")
	private String name;
	@Value("111")
	private String id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
