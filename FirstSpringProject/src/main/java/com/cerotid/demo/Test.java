package com.cerotid.demo;

import org.springframework.context.support.FileSystemXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext("beans.xml");
		/*
		 * HelloWorld hello = (HelloWorld) context.getBean(HelloWorld.class);
		 * hello.sayHello();
		 * 
		 * 
		 * Person person = (Person) context.getBean("person1");
		 * System.out.println(person.toString());
		 */
		
		User user = (User) context.getBean("user");
		System.out.println(user.getCar().toString());
		
		
		context.close();

	}

}
