package com.cerotid.demo;

import org.springframework.beans.factory.annotation.Autowired;

public class B_AutoWiredExample {
	private A_AutoWiredExample a;
	

	public A_AutoWiredExample getA() {
		return a;
	}

	@Autowired
	public void setA(A_AutoWiredExample a) {
		this.a = a;
	}
	
	public void bMethod() {
		System.out.println("This is class B_AutoWiredExample Method");
		a.aMethod();
	}
	

}
