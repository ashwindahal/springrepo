package com.cerotid.demo;

import org.springframework.context.support.FileSystemXmlApplicationContext;

public class AutoWiredTest {

	public static void main(String[] args) {
		//ask why this is not working??
		//ApplicationContext context = new ClassPathXmlApplicationContext("autoWiredApplicationContext.xml");
		FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext("autoWiredApplicationContext.xml");
		B_AutoWiredExample b = (B_AutoWiredExample) context.getBean("b");
		b.bMethod();
		
		
		StudentAutoWiredQualifierExample student = (StudentAutoWiredQualifierExample) context.getBean("studentWithHostel");
		System.out.println(student);
		
		context.close();
		
		
	}

}
