package com.cerotid.demo;

import org.springframework.context.support.FileSystemXmlApplicationContext;

public class HostelTest {

	public static void main(String[] args) {
		/*
		 * Student student = new Student(); student.setName("Ashwin Dahal");
		 * student.setId("123456789");
		 * 
		 * Hostel hostel = new Hostel(); hostel.setHostelName("American Hostel");
		 * hostel.setCity("Dallas");
		 * 
		 * student.setHostel(hostel);
		 * 
		 * 
		 * System.out.println(student.toString());
		 */

		FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext("beans2.xml");
		/*
		 * Student student = (Student) context.getBean("student");
		 * System.out.println(student.toString());
		 */

		Student student2 = (Student) context.getBean("student");
		System.out.println(student2.toString());

		context.close();

	}

}
