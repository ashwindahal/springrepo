package com.cerotid.demo;

import org.springframework.context.support.FileSystemXmlApplicationContext;

public class ComponentTest {
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext("ComponentTest.xml");
		ComponentExample hello = (ComponentExample) context.getBean("componentExample");
		hello.sayHello();

	}

}
