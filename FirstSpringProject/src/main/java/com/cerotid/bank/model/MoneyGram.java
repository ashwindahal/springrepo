package com.cerotid.bank.model;


public class MoneyGram extends Transaction {
	private DeliveryOption deliveryOptions;
	private String destinationCountry;

	public DeliveryOption getDeliveryOptions() {
		return deliveryOptions;
	}

	public void setDeliveryOptions(DeliveryOption deliveryOptions) {
		this.deliveryOptions = deliveryOptions;
	}

	public String getDestinationCountry() {
		return destinationCountry;
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}

	@Override
	public String toString() {
		return super.toString() + " MoneyGram [deliveryOptions=" + deliveryOptions + ", destinationCountry=" + destinationCountry + "]";
	}
	

}
