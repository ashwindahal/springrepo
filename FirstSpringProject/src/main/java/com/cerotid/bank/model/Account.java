package com.cerotid.bank.model;

import java.io.Serializable;
import java.util.Date;

public class Account implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4105532199783587880L;
	private AccountType accountType;
	private Date accountOpenDate;
	private Date accountCloseDate;
	private double amount;
	private Date date;

	public Account(AccountType accountType, Date date, double accountBalance) {
		this.accountType = accountType;
		this.setDate(date);
		this.amount = accountBalance; 
		
	}

	// Accessors and modifiers (Encapsulation == giving the access level to user)
	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public Date getAccountOpenDate() {
		return accountOpenDate;
	}

	public void setAccountOpenDate(Date accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}

	public Date getAccountCloseDate() {
		return accountCloseDate;
	}

	public void setAccountCloseDate(Date accountCloseDate) {
		this.accountCloseDate = accountCloseDate;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	// behaviors
	void printAccountInfo() {
		System.out.println(toString());
	}

	public void sendMoney() {
		// TODO
		// - Provide option to choose account
		// - provide option to choose type of Transation to initiate
		// - Complete the transation
	}

	@Override
	public String toString() {
		return "Account [accountType=" + accountType + ", accountOpenDate=" + accountOpenDate + ", accountCloseDate="
				+ accountCloseDate + ", amount=" + amount + "]";
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
