package com.cerotid.bank.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

class ReadFile extends Thread {
	public void run() {
		try {
			Scanner reader = new Scanner(new File("Customers.txt"));
			while (reader.hasNext()) {
				reader.nextLine();
				String customerLine = reader.nextLine();
				System.out.println(customerLine);
				// start thread 1 reader, 2 processor, 3

			}
		} catch (FileNotFoundException e) {
			System.out.println("File not Found, " + e.getMessage());
			e.printStackTrace();
		}
	}
}

class Processor extends Thread {
	public void run() {
		/*
		 * ArrayList<Customer> customerList = new ArrayList<Customer>(); Address address
		 * = new Address(customerInfos[2], customerInfos[3], customerInfos[4],
		 * customerInfos[5]); Customer customer = new Customer(customerInfos[0],
		 * customerInfos[1], null, address); customerList.add(customer);
		 */
		
		System.out.println("Hello");
	}
}

class WriteFile extends Thread {
	/*
	 * ileWriter writer = new FileWriter("Customers.txt"); if (customerList != null)
	 * { for (Customer customer : customerList) {
	 * writer.write(customer.getFirstName() + "," + customer.getLastName() + "," +
	 * customer.getAddress() + "\n"); }
	 * 
	 * } writer.close();
	 */
}

public class JavaThreadTest {
	public static void main(String args[]) {

		ReadFile readFile = new ReadFile();
		readFile.start();

		Processor processor = new Processor();
		processor.start();

	}

}
