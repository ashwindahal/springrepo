package com.cerotid.bank.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class CustomerReader {
	ArrayList<Customer> customerList = new ArrayList<Customer>();
	Scanner reader;

	CustomerReader() {
		initialize("Customers.txt");
	}

	// overloaded constructor
	CustomerReader(String fileName) {
		initialize(fileName);

	}

	private void initialize(String fileName) {
		try {
			reader = new Scanner(new File(fileName));
		} catch (FileNotFoundException e) {
			System.out.println("File not Found, " + e.getMessage());
			e.printStackTrace();
		}
	}

	// create method to read file
	public void readFile() {
		while (reader.hasNext()) {
			String customerLine = reader.next();
			String[] splitStrings = customerLine.split(",");
			addCustomer(splitStrings);

		}
		reader.close();
	}

	// create writer file
	public void writeFile() throws IOException {
		sortCustomers(customerList);
		FileWriter writer = new FileWriter("CustomerOutputFile.txt");
		if (customerList != null) {
			for (Customer customer : customerList) {
				writer.write(customer.getFirstName() + "," + customer.getLastName() + "," + customer.getAddress() + "\n");
			}

		}
		writer.close();
	}

	// sorts an ArrayList of type Customer using "Collections.sort"
	private void sortCustomers(ArrayList<Customer> customerList) {
		Collections.sort(customerList, (o1, o2) -> {
			Customer c1 = (Customer) o1;
			Customer c2 = (Customer) o2;
			int res = c1.getFirstName().compareToIgnoreCase(c2.getFirstName());
			if (res != 0)
				return res;
			return c1.getLastName().compareToIgnoreCase(c2.getLastName());
		});
	}

	// Clean Code
	public void addCustomer(String[] customerNameArray) {
		Customer cus = new Customer(customerNameArray[1], customerNameArray[0], null, null);
		customerList.add(cus);
	}

}
