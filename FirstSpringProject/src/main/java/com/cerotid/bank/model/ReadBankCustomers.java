package com.cerotid.bank.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;

public class ReadBankCustomers {
	static String customerLine;
	static List<String> customerInfoStringList;
	static ArrayList<Customer> customerList;
	static Customer customer;

	private static final Object lock = new Object();

	static Bank bank;

	// Dependency Injection
	public ReadBankCustomers(Bank bank) {
		ReadBankCustomers.bank = bank;
	}

	public void loadBank() {

		Thread t1 = new Thread(new Runnable() {
			public void run() {
				try {
					readFile("Customers.txt");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});

		Thread t2 = new Thread(new Runnable() {
			public void run() {
				try {
					customerList = new ArrayList<Customer>();
					synchronized (lock) {
						for (String customerLine : customerInfoStringList) {
							String[] customerInfos = customerLine.split(",");
							processor(customerInfos);
							System.out.println(customerLine);
						}
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});

		Thread t3 = new Thread(new Runnable() {
			public void run() {
				try {
					writeFile();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		t1.start();
		try {
			t1.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		t2.start();
		try {
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		t3.start();
		try {
			t3.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// 1 reader, 2 processor, 3 writer
	private static void readFile(String fileName) throws InterruptedException {
		try {
			Scanner reader = new Scanner(new File(fileName));

			customerInfoStringList = new Vector<String>();
			while (reader.hasNext()) {
				synchronized (lock) {
					customerLine = reader.nextLine();

					customerInfoStringList.add(customerLine);
				}

			}
			reader.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not Found, " + e.getMessage());
			e.printStackTrace();
		}
	}

	private static void processor(String[] customerInfos) throws InterruptedException {
		Address address = new Address(customerInfos[2], customerInfos[3], customerInfos[4], customerInfos[5]);
		Customer customer = new Customer(customerInfos[0], customerInfos[1], customerInfos[2], address);
		customerList.add(customer);

	}

	public static void writeFile() throws IOException {
		FileWriter writer = new FileWriter("AllCustomers.txt");

		if (customerList != null) {
			for (Customer customer : customerList) {
				writer.write(customer.getFirstName() + "," + customer.getLastName() + "," + customer.getSsn() + ","
						+ customer.getAddress() + "\n");

			}
			assignToBankCustomerArray();
		}
		writer.close();

	}

	private static void assignToBankCustomerArray() {
		bank.setCustomers(customerList);
	}

}