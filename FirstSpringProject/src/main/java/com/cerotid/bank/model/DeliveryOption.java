package com.cerotid.bank.model;

public enum DeliveryOption {
	// this will not change
	TEN_MINUTES("10Min"), TWENTY_FOUR_HOURS("24HRS");

	// have to define "DeliveryOption" with a final variable
	private final String DeliveryTime;

	DeliveryOption(String DeliveryTime) {
		this.DeliveryTime = DeliveryTime;
	}

	public String getDeliveryTime() {
		return DeliveryTime;
	}

}
