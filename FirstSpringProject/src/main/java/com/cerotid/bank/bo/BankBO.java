package com.cerotid.bank.bo;

import java.util.List;

import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.Bank;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.model.Transaction;

public interface BankBO {
	public void addCustomer(Customer customer);
	public boolean openAccount(Customer customer, Account account);
	public void sendMoney(Customer customer, Transaction transaction);
	public void depositMoneyInCustomerAccount(Customer customer);
	public void editCustomerInfo(Customer customer);
	public Customer getCustomerInfo(String ssn);
	public void printBankStatus();
	public List<Customer> getCustomersByState(String stateCode);//opens a Pandora box of COllection Framework Understanding
	public Bank getBank();
}
